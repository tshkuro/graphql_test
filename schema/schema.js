const graphql = require('graphql');
// import graphql from 'graphql';
const url = "mongodb+srv://tanya:<shkuro>@cluster0-gbrad.mongodb.net/test?retryWrites=true&w=majority"

let movies = [
    { id: '1', name: 'Pulp Fiction', genre: 'Crime', directorId: '1', },
    { id: '2', name: '1984', genre: 'Sci-Fi', directorId: '2', },
    { id: '3', name: 'V for vendetta', genre: 'Sci-Fi-Triller', directorId: '3', },
    { id: '4', name: 'Snatch', genre: 'Crime-Comedy', directorId: '4', },
    { id: '5', name: 'Reservoir Dogs', genre: 'Crime', directorId: '1' },
    { id: '6', name: 'The Hateful Eight', genre: 'Crime', directorId: '1' },
    { id: '7', name: 'Inglourious Basterds', genre: 'Crime', directorId: '1' },
    { id: '8', name: 'Lock, Stock and Two Smoking Barrels', genre: 'Crime-Comedy', directorId: '4' },
];

let directors = [
    { id: '1', name: 'Quentin Tarantino', age: 55 },
    { id: '2', name: 'Michael Radford', age: 72 },
    { id: '3', name: 'James McTeigue', age: 51 },
    { id: '4', name: 'Guy Ritchie', age: 50 },
];

const { 
    GraphQLObjectType, 
    GraphQLSchema, 
    GraphQLNonNull, 
    GraphQLString,
    GraphQLID, 
    GraphQLInt, 
    GraphQLList 
} = graphql;

const MovieType = new GraphQLObjectType({
    name: 'Movie',
    fields: () => ({
        id: { type: GraphQLID },
        name: { type: GraphQLString },
        genre: { type: GraphQLString },
        director: {
            type: DirectorType,
            resolve(parent, args) {
                return directors.find(dir => dir.id === parent.id);
            }
        }
    }),
});

const DirectorType = new GraphQLObjectType({
    name: 'Director',
    fields: () => ({
        id: { type: GraphQLID },
        name: { type: GraphQLString },
        age: { type: GraphQLInt },
        movies: {
            type: new GraphQLList(MovieType),
            resolve(parent, args) {
                return movies.filter(movie => movie.directorId === parent.id);
            }
        }
    }),
});

const Mutation = new GraphQLObjectType({
    name: "Mutation",
    fields: {
        addDirector: {
            type: DirectorType,
            args: {
                name: { type: new GraphQLNonNull(GraphQLString) },
                age: { type: new GraphQLNonNull(GraphQLInt) },
            },
            resolve(parent, args) {
                directors.push({ name: args.name, age: args.age, id: (directors.length + 1).toString() });
                return directors[directors.length - 1];
            }
        },
        deleteDirector: {
            type: DirectorType,
            args: { id: { type: new GraphQLNonNull(GraphQLID) } },
            resolve(parent, args) {
                const dir = { ...directors.find(dir => dir.id == args.id) };
                directors = directors.filter(dir => dir.id != args.id);
                return dir;
            }
        },
        updateDirector: {
            type: DirectorType,
            args: {
                id: { type: new GraphQLNonNull(GraphQLID) },
                name: { type: GraphQLString },
                age: { type: GraphQLInt },
            },
            resolve(parent, args) {
                let director = null;
                directors = directors.map(dir => {
                    if (dir.id == args.id) {
                        if (args.name) {
                        dir.name = args.name;
                        }
                        if (args.age) {
                            dir.age = args.age;
                        }
                        director = dir;
                    }
                    return dir;
                });
                return director;
            }
        },
        addMovie: {
            type: MovieType,
            args: {
                name: { type: new GraphQLNonNull(GraphQLString) },
                genre: { type: new GraphQLNonNull(GraphQLString) },
                directorId: { type: new GraphQLNonNull(GraphQLID) },
            },
            resolve(parent, args) {
                movies.push({
                    name: args.name,
                    genre: args.genre,
                    directorId: args.directorId,
                    id: (movies.length + 1).toString()
                });
                return movies[movies.length - 1];
            }
        },
        deleteMovie: {
            type: MovieType,
            args: { id: { type: new GraphQLNonNull(GraphQLID) } },
            resolve(parent, args) {
                const mov = movies.find(movie => movie.id == args.id);
                movies = movies.filter(movie => movie.id != args.id);
                return mov;
            }
        },
        updateMovie: {
            type: MovieType,
            args: {
                id: { type: new GraphQLNonNull(GraphQLID) },
                name: { type: GraphQLString },
                genre: { type: GraphQLString },
                directorId: {type: GraphQLID },
            },
            resolve(parent, args) {
                let movie = null;
                movies = movies.map(mov => {
                    if (mov.id == args.id) {
                        if (args.name) {
                        mov.name = args.name;
                        }
                        if (args.genre) {
                            mov.genre = args.genre;
                        }
                        if (args.directorId) {
                            mov.directorId = args.directorId;
                        }
                        movie = mov;
                    }
                    return mov;
                });
                return movie;
            }
        },
    }
})

const Query = new GraphQLObjectType({
    name: 'Query',
    fields: {
        movie: {
            type: MovieType,
            args: { id: { type: GraphQLID } },
            resolve(parent, args) {
                return movies.find(movie => movie.id == args.id);
            }
        },
        director: {
            type: DirectorType,
            args: { id: { type: GraphQLID } },
            resolve(parent, args) {
                return directors.find(dir => dir.id == args.id);
            }
        },
        movies: {
            type: new GraphQLList(MovieType),
            resolve(parent, args) {
                return movies;
            }
        },
        directors: {
            type: new GraphQLList(DirectorType),
            resolve(parent, args) {
                return directors;
            }
        }
    },
});

module.exports = new GraphQLSchema({
    query: Query,
    mutation: Mutation
});

