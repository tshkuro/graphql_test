import {gql } from 'apollo-boost';

export const moviesQuery = gql`
query moviesQuery {
    movies {
      name
      id
      genre
    }
  }
`;

export const movieQuery = gql`
    query movieQuery($id: ID!) {
        movie(id: $id) {
            id
            name
            genre
            director {
                name
                age
            }
        }
    }
`;

export const directorsQuery = gql`
    query directorsQuery {
        directors {
            id
            name
            age
        }
    }
`;

export const directorQuery = gql`
    query directorQuery($id: ID!) {
        director(id: $id) {
            id
            name
            age
            movies {
                name
            }
        }
    }
`;