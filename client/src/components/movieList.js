import React from 'react';
import { graphql } from 'react-apollo';
import { moviesQuery } from './queries';
import { compose } from 'recompose';
import { Grid } from '@material-ui/core';

class MovieList extends React.Component {
    render() {
        console.log(this.props.data.movies);
        let movies = null;
        if (!this.props.data.loading) {
            movies = this.props.data.movies.map(movie => (
                <Grid item key={movie.id}>
                    <div className="movie-li">
                        <span>{`Name: ${movie.name}`}</span><br></br>
                        <span>{`genre: ${movie.genre}`}</span>
                    </div>

                </Grid>
            ));
        }
        return (
            <Grid container spacing={1}>
                {movies}
            </Grid>
        );
    }
}

export default graphql(moviesQuery)(MovieList);