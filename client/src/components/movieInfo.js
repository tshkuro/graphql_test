import React from 'react';
import { graphql } from 'react-apollo';
import { movieQuery } from './queries';
import { compose } from 'recompose';
import { Grid, Paper } from '@material-ui/core';


class MovieInfo extends React.Component {
    render() {
        const movie = this.props.data.movie;
        return (
            <Paper elevation={3} className="item-info">
                {movie && <>
                    <span>{`Name: ${movie.name}`}</span><br></br>
                    <span>{`Genre: ${movie.genre}`}</span><br></br>
                    <span>{`Director: ${movie.director.name}`}</span><br></br>
                </>
                }

            </Paper>
        );
    }
}

export default graphql(movieQuery, {
    options: (props) => {
        return {
            variables: {
                id: props.id
            }
        }
    }
})(MovieInfo);