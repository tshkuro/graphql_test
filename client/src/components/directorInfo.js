import React from 'react';
import { graphql } from 'react-apollo';
import { directorQuery } from './queries';
import { Grid, Paper } from '@material-ui/core';


class DirectorInfo extends React.Component {
    render() {
        const director = this.props.data.director;
        return (
            <Paper elevation={3} className="item-info">
                {director && <>
                    <span>{`Name: ${director.name}`}</span><br></br>
                    <span>{`Age: ${director.age}`}</span><br></br>
                    <span>Movies:</span>
                    <ul>
                        {director.movies.map(movie => (
                            <li>{movie.name}</li>
                        ))}
                    </ul>
                </>}
            </Paper>
        );
    }
}

export default graphql(directorQuery, {
    options: (props) => {
        return {
            variables: {
                id: props.id
            }
        }
    }
})(DirectorInfo);