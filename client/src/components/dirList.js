import React from 'react';
import { graphql } from 'react-apollo';
import { directorsQuery } from './queries';
import { compose } from 'recompose';
import { Grid } from '@material-ui/core';

class DirList extends React.Component {
    render() {
        console.log(this.props.data.directors);
        let directors = null;
        if (!this.props.data.loading) {
            directors = this.props.data.directors.map(dir => (
                <Grid item key={dir.id}>
                    <div className="director-li">
                        <span>{`Name: ${dir.name}`}</span><br></br>
                        <span>{`age: ${dir.age}`}</span>
                    </div>
                </Grid>
            ));
        }
        return (
            <Grid container spacing={1}>
                {directors}
            </Grid>
        );
    }
}

export default graphql(directorsQuery)(DirList);
