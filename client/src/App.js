import React from 'react';
// import logo from './logo.svg';
import './App.css';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import { Grid } from '@material-ui/core';
import MovieList from './components/movieList';
import DirList from './components/dirList';
import MovieInfo from './components/movieInfo';
import DirectorInfo from './components/directorInfo';


const client = new ApolloClient({
  uri: 'http://localhost:3005/graphql',
  onError: ({ graphQLErrors, networkError, operation, forward }) => {
    if (graphQLErrors) {
      for (let err of graphQLErrors) {
        console.log("GraphQL Error", err);
      }
    }
    console.log("networkError", networkError);
    console.log("operation", operation);
    console.log("forward", forward);
  }
})

function App() {
  return (
    <ApolloProvider client={client}>
      <Grid container>
        <Grid item xs={6}>
          <h3>Movies</h3>
          <MovieList />
        </Grid>
        <Grid item xs={6}>
          <h3>Directors</h3>
          <DirList />
        </Grid>
        <Grid item xs={6}>
          <MovieInfo
            id={'1'}
          />
        </Grid>
        <Grid item xs={6}>
          <DirectorInfo
            id={'1'}
          />
        </Grid>
      </Grid>
    </ApolloProvider>
  );
}

export default App;
