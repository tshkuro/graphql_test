const express = require('express')
const graphqlHTTP = require('express-graphql')
const schema = require('../schema/schema');
const cors = require('cors');

const app = express();
const PORT = 3005;

// app.all('*', (req, res) => {
//     console.log('app', req)
// });


app.use(cors());

app.use('/graphql', graphqlHTTP({
    schema, graphiql: true
}));

app.use((err, req, res, next) => {
    console.log("APP SERVER")
    console.log(err);
    console.log(req);
    console.log(res);
    console.log(next);
    // const err = new Error('Error. Sadly.');
    // err.status = 404;
    // next(err);
})

app.listen(PORT, err => {
    err ? console.log(err) : console.log('Server started')
});

